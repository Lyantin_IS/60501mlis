<?php namespace App\Models;
use CodeIgniter\Model;
class polling extends Model
{
    protected $table= 'polling_station'; //таблица, связанная с моделью
    protected $allowedFields = ['voter_number', 'station_number', 'id_region', 'id', 'picture_url'];       
    public function getPolling($id){
                       
                return $this->where('id', $id)->first();
            
        }

}
