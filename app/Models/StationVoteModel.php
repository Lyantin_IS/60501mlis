<?php namespace App\Models;
use CodeIgniter\Model;
class StationVoteModel extends Model
{
    protected $table= 'region'; //таблица, связанная с моделью
    protected $allowedFields = ['voter_number', 'station_number', 'id'];
    public function getStationVote($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        else {                       
            return $this->select('l.id, l.id_region, r.name, l.station_number, l.voter_number, l.picture_url')->distinct()->from('polling_station l')->join('region r', 'l.id_region=r.id')->where('l.id_region', $id)->findAll();
        }
    }

    public function getStationVoteAdmin($id = null, $search = '')
    {    
        $builder = $this->select('l.id, l.id_region, r.name, l.station_number, l.voter_number, l.picture_url')->distinct()->from('polling_station l')->join('region r', 'l.id_region=r.id');     
        if (!is_null($id)) {
            return $builder->where('l.id', $id)->first();
        }
             
        return $builder->like('r.name', $search,'both', null, true)->orlike('r.name',$search,'both',null,true);
    }
}


