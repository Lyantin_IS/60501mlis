<?php namespace App\Controllers;

use App\Models\StationVoteModel;
use App\Models\polling;
use Aws\S3\S3Client;
//use CodeIgniter\Controller;

class StationVoteController extends BaseController
{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            //return redirect()->to('/auth/login');
        }
        $model = new StationVoteModel();
        $data ['region'] = $model->getStationVote();

        echo view('StationVote/StationVoteViewAll', $this->withIon($data));

    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            //return redirect()->to('/auth/login');
        }
        $model = new StationVoteModel();
        $data ['polling_station'] = $model->getStationVote($id);

        echo view('StationVote/StationVoteView', $this->withIon($data));

    }

    public function viewAdmin()
    {
        if ($this->ionAuth->isAdmin())
        {
                    //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

//Обработка запроса на поиск
        if (!is_null($this->request->getPost('search')))
        {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        }
        else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;

            helper(['form','url']);

            $model = new StationVoteModel();
            $data['all'] = $model->getStationVoteAdmin(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('StationVote/StationVoteViewAllAdmin', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Необходимы права администратора'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        
        $model = new StationVoteModel();
        $data ['region'] = $model->getStationVote();

        echo view('StationVote/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_region' => 'required',
                'station_number'  => 'required|min_length[5]|max_length[7]',
                'voter_number'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            
             $insert = null;
        //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }       

            $model = new polling();
            $data = [
                'id_region' => $this->request->getPost('id_region'),
                'station_number' => $this->request->getPost('station_number'),
                'voter_number' => $this->request->getPost('voter_number'),
            ];

            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Curating.StationVote_create_success'));
            return redirect()->to('/StationVoteController');
        }
        else
        {
            return redirect()->to('/StationVote/create')->withInput();
        }
    }


    public function edit($id){
        if (!$this->ionAuth->loggedIn()){
            return redirect()->to('/auth/login');
        }
        $model = new polling();
        helper(['form']);
        $data ['polling_station'] = $model->getpolling($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('StationVote/edit', $this->withIon($data));
    }
    public function update()
    {
        helper(['form','url']);
        echo '/StationVoteController/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'id_region' => 'required',
                'station_number'  => 'required',
                'voter_number'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]'
            ]))
        {

            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
            //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);            
                //получение расширения имени загруженного файла
            $ext = explode('.', $file->getName());
            $ext = $ext[count($ext) - 1];
            //загрузка файла в хранилище
            $insert = $s3->putObject([
                'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                //генерация случайного имени файла
                'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                'Body' => fopen($file->getRealPath(), 'r+')
            ]);
        }                

            $model = new polling();
            $data = [
                'id' => $this->request->getPost('id'),
                'id_region' => $this->request->getPost('id_region'),
                'station_number' => $this->request->getPost('station_number'),
                'voter_number' => $this->request->getPost('voter_number'),
            ];

            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));


            return redirect()->to('/StationVoteController');
        }
        else
        {
           return redirect()->to('/StationVoteController/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new polling();
        $model->delete($id);
        return redirect()->to('/StationVoteController');
    }


}
