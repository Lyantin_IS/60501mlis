<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class m60501lis extends Seeder
{
    public function run()
   {
        $data = [
                'FIO' => 'Путин В.В.',
        ];
        $this->db->table('candidate')->insert($data);
        $data = [
                'FIO' => 'Навальный А.А.',
        ];
        $this->db->table('candidate')->insert($data);
        $data = [
                'FIO' => 'Жириновский В.В.',
        ];
        $this->db->table('candidate')->insert($data);
        $data = [
                'FIO' => 'Собчак К.А.',
        ];
        $this->db->table('candidate')->insert($data);
        $data = [
                'FIO' => 'Против всех',
        ];
        $this->db->table('candidate')->insert($data);
        $data = [
                'FIO' => 'Недействительный голос',
        ];
        $this->db->table('candidate')->insert($data);

    //-----------------------------------------------
        $data = [
                'name' => 'Еврейская АО',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Ханты-Мансийский АО',
        ];
        $this->db->table('region')->insert($data);  
        $data = [
                'name' => 'Ямало-Ненецкий АО',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Москововская обл.',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Ленинградская обл.',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Свердловская обл.',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Краснодарский кр.',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Крым респ.',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Москва',
        ];
        $this->db->table('region')->insert($data);
        $data = [
                'name' => 'Санкт-Петербург',
        ];
        $this->db->table('region')->insert($data);

     //-----------------------------------------------
        $data = [
                'id_region' => '1','station_number' => '777777','voter_number' => '13638'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '2','station_number' => '128567','voter_number' => '15001'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '3','station_number' => '101010','voter_number' => '64213'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '4','station_number' => '999999','voter_number' => '18560'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '5','station_number' => '648234','voter_number' => '18903'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '6','station_number' => '845287','voter_number' => '31001'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '7','station_number' => '762893','voter_number' => '19766'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '8','station_number' => '654321','voter_number' => '15676'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '9','station_number' => '101010','voter_number' => '29809'
        ];
        $this->db->table('polling_station')->insert($data);
$data = [
                'id_region' => '10','station_number' => '123456','voter_number' => '20067'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '9','station_number' => '987654','voter_number' => '12345'
        ];
        $this->db->table('polling_station')->insert($data);
        $data = [
                'id_region' => '10','station_number' => '456789','voter_number' => '13765'
        ];
        $this->db->table('polling_station')->insert($data);
$data = [
                'id_region' => '1','station_number' => '99999','voter_number' => '12'
        ];
        $this->db->table('polling_station')->insert($data);

        //-----------------------------------------------
        $data = [
                'id_polling_station' => '1','id_candidate' => '2','vote_number' => '7000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '3','id_candidate' => '4','vote_number' => '5000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '4','id_candidate' => '1','vote_number' => '8000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '5','id_candidate' => '2','vote_number' => '9000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '6','id_candidate' => '4','vote_number' => '8000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '7','id_candidate' => '1','vote_number' => '8000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '8','id_candidate' => '2','vote_number' => '6700'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '9','id_candidate' => '4','vote_number' => '18900'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '10','id_candidate' => '4','vote_number' => '12000'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '1','id_candidate' => '3','vote_number' => '3475'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '1','id_candidate' => '4','vote_number' => '0'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '1','id_candidate' => '5','vote_number' => '123'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '1','id_candidate' => '6','vote_number' => '7'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '1','id_candidate' => '1','vote_number' => '33'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '3','id_candidate' => '1','vote_number' => '32'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '3','id_candidate' => '2','vote_number' => '235'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '3','id_candidate' => '3','vote_number' => '708'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '3','id_candidate' => '5','vote_number' => '45774'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '3','id_candidate' => '6','vote_number' => '3465'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '4','id_candidate' => '2','vote_number' => '523'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '4','id_candidate' => '3','vote_number' => '3567'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '4','id_candidate' => '4','vote_number' => '23'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '4','id_candidate' => '5','vote_number' => '685'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '4','id_candidate' => '6','vote_number' => '12'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '5','id_candidate' => '1','vote_number' => '0'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '5','id_candidate' => '3','vote_number' => '1'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '5','id_candidate' => '4','vote_number' => '12'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '5','id_candidate' => '5','vote_number' => '124'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '5','id_candidate' => '6','vote_number' => '4366'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '6','id_candidate' => '1','vote_number' => '1542'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '6','id_candidate' => '2','vote_number' => '2531'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '6','id_candidate' => '3','vote_number' => '66'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '6','id_candidate' => '5','vote_number' => '6345'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '6','id_candidate' => '6','vote_number' => '9767'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '7','id_candidate' => '2','vote_number' => '324'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '7','id_candidate' => '3','vote_number' => '1245'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '7','id_candidate' => '4','vote_number' => '7685'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '7','id_candidate' => '5','vote_number' => '436'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '7','id_candidate' => '6','vote_number' => '76'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '8','id_candidate' => '1','vote_number' => '537'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '8','id_candidate' => '3','vote_number' => '83'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '8','id_candidate' => '4','vote_number' => '3568'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '8','id_candidate' => '5','vote_number' => '34'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '8','id_candidate' => '6','vote_number' => '754'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '9','id_candidate' => '1','vote_number' => '5467'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '9','id_candidate' => '2','vote_number' => '94'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '9','id_candidate' => '3','vote_number' => '346'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '9','id_candidate' => '5','vote_number' => '568'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '9','id_candidate' => '6','vote_number' => '34'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '10','id_candidate' => '1','vote_number' => '1'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '10','id_candidate' => '2','vote_number' => '4326'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '10','id_candidate' => '3','vote_number' => '758'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '10','id_candidate' => '5','vote_number' => '56'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '10','id_candidate' => '6','vote_number' => '77'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '2','id_candidate' => '1','vote_number' => '2356'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '2','id_candidate' => '2','vote_number' => '6598'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '2','id_candidate' => '3','vote_number' => '356'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '2','id_candidate' => '4','vote_number' => '2349'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '2','id_candidate' => '5','vote_number' => '234'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '2','id_candidate' => '6','vote_number' => '543'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '11','id_candidate' => '1','vote_number' => '5657'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '11','id_candidate' => '2','vote_number' => '345'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '11','id_candidate' => '3','vote_number' => '234'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '11','id_candidate' => '4','vote_number' => '577'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '11','id_candidate' => '5','vote_number' => '456'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '11','id_candidate' => '6','vote_number' => '34'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '12','id_candidate' => '1','vote_number' => '2986'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '12','id_candidate' => '2','vote_number' => '3467'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '12','id_candidate' => '3','vote_number' => '17'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '12','id_candidate' => '4','vote_number' => '3677'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '12','id_candidate' => '5','vote_number' => '76'
        ];
        $this->db->table('vote_results')->insert($data);
        $data = [
                'id_polling_station' => '12','id_candidate' => '6','vote_number' => '32'
        ];
        $this->db->table('vote_results')->insert($data);
   }

}

