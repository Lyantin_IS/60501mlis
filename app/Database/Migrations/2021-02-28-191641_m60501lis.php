<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class m60501lis extends Migration
{
	public function up()
	{
		if (!$this->db->tableexists('candidate'))
        {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'FIO' => array('type' => 'VARCHAR', 'constraint' => '40', 'null' => FALSE)
            ));

            $this->forge->createtable('candidate', TRUE);
        }

        //-------------------------------------
        if (!$this->db->tableexists('region'))
        {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '40', 'null' => FALSE)
            ));

            $this->forge->createtable('region', TRUE);
        }

        //-------------------------------------
        if (!$this->db->tableexists('polling_station'))
        {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_region' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'station_number' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'voter_number' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));

            $this->forge->addForeignKey('id_region', 'region', 'id', 'RESTRICT', 'RESTRICT');

            $this->forge->createtable('polling_station', TRUE);
        }

        //-------------------------------------
        if (!$this->db->tableexists('vote_results'))
        {
            $this->forge->addfield(array(
                'id_polling_station' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_candidate' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'vote_number' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));

            $this->forge->addForeignKey('id_polling_station', 'polling_station', 'id', 'RESTRICT', 'RESTRICT');
            $this->forge->addForeignKey('id_candidate', 'candidate', 'id', 'RESTRICT', 'RESTRICT');

            $this->forge->createtable('vote_results', TRUE);
        }
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->droptable('candidate');
        $this->forge->droptable('region');
        $this->forge->droptable('polling_station');
        $this->forge->droptable('vote_results');
	}
}
