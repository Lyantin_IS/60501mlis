<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<main class="text-center">
    <div class="jumbotron">
        <img class="mb-4" src="https://www.flaticon.com/svg/static/icons/svg/1867/1867971.svg" alt="" width="92" height="92"><h1 class="display-4">ВЫБОРЫ 2021</h1>
        <p class="lead">Добро пожаловать на сайт ВЫБОРЫ 2021. Вы можете ознакомиться с списком избирательных участков в регионах.</p>
        <!--<a class="btn btn-primary btn-lg" href="#" role="button">Войти</a> -->
    </div>
</main>

<?= $this->endSection() ?>
