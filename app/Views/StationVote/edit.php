<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

<?= form_open_multipart('StationVoteController/update'); ?>
<input type="hidden" name="id" value="<?= $polling_station["id"] ?>">
<input type="hidden" name="id_region" value="<?= $polling_station["id_region"] ?>">

<div class="form-group">
<label for="station_number">Номер участка</label>
<input type="number" min="99999" class="form-control <?= ($validation->hasError('station_number')) ? 'is-invalid' : ''; ?>" name="station_number"
value="<?= $polling_station["station_number"]; ?>">
<div class="invalid-feedback">
<?= $validation->getError('station_number') ?>
</div>

</div>
<div class="form-group">
<label for="voter_number">Количество прикрепленных к участку избирателей</label>
<input type="number" min="1" class="form-control <?= ($validation->hasError('voter_number')) ? 'is-invalid' : ''; ?>" name="voter_number"
value="<?= $polling_station["voter_number"]; ?>">
<div class="invalid-feedback">
<?= $validation->getError('voter_number') ?>
</div></div>

<div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture" value="<?= $polling_station["picture_url"] ?>" >
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
 </div>

<br>
<div class="form-group">
<button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
</div>
</form>
</div>
<?= $this->endSection() ?>
