<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div  class="container">
<h1>Все регионы</h1>
    <br><h5> На данной странице представлены регионы в которых проводится голосование. </h5></br>
<?php if (!empty($region) && is_array($region)) : ?>
    
    <table class="table">
    <thead class="text-white bg-primary">
        <tr>
            <th scope="col"> #</th>
            <th scope="col"> Регион</th>
            <th scope="col"> Подробнее</th>
           </tr>
    </thead>
    <?php
    $c=1; 
    foreach ($region as $item): ?>
    <tbody>
        <tr>
            <th scope='row'><?php echo $c ?></th>
            <td><?php echo $item['name'] ?></td>
            <td><a href="<?= base_url()?>/index.php/StationVoteController/view/<?= esc($item['id']); ?>" class="btn btn-outline-primary"> Посмотреть</a></td>
       </tr> 
    <?php $c+=1; endforeach; ?> 
    </tbody>
    </table>
    <?php else : ?>
        <p>Невозможно найти регионы.</p>
    <?php endif ?>
<br>
<br>
</div>

<?= $this->endSection() ?>
