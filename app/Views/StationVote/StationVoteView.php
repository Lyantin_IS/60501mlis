<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container">
<h1>Избирательные участки</h1>
    <br><h5>На данной странице представлены избирательные участки выбранного региона в которых проводится голосование.</h5></br>
    <?php if (!empty($polling_station) && is_array($polling_station)) : ?>
<?php foreach ($polling_station as $item): ?>
             <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">   
                        <?php if (is_null($item['picture_url'])) : ?>
                            <a> &nbsp;&nbsp;&nbsp;&nbsp; </a>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg" class="card-img">
                        <?php else:?>
                            <a> &nbsp;&nbsp;&nbsp;&nbsp; </a>
                            <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Регион: <?= esc($item['name']); ?></h5>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Номер региона:</div>
                                <div class="text-muted"><?= esc($item['id_region']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Номер участка:</div>
                                <div class="text-muted"><?= esc($item['station_number']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Количество избирателей прикрепленных к участку:</div>
                                <div class="text-muted"><br><?= esc($item['voter_number']); ?></div>
                            </div>
                                
                                    <?php if ( $ionAuth->loggedIn()): ?>  
                                            <br>
                                            <a class="btn btn-warning " href="<?= base_url()?>/index.php/StationVoteController/edit/<?php echo $item['id'] ?>">Редактировать</a>
                                            <a class="btn btn-danger " href="<?= base_url()?>/index.php/StationVoteController/delete/<?php echo $item['id'] ?>">Удалить</a>
                                     <?php endif ?>    

                            <div class="d-flex justify-content-between"></div>
                        </div>
                    </div> 
                </div> 
            </div>
<?php endforeach; ?>
    <?php else : ?>
        <p>Участки не найдены.</p>
    <?php endif ?>
<br>
<br>
</div>

<?= $this->endSection() ?>
