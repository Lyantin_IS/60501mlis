<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('StationVoteController/store'); ?>
    <div class="form-group">
        <label for="id_region">Регион</label>
        <select type="text" class="form-control <?= ($validation->hasError('id_region')) ? 'is-invalid' : ''; ?>" name="id_region"
               value="<?= old('id_region'); ?>">
            <?php foreach ($region as $item): {
                echo "<option value=" . $item['id'] . ">" . $item['name'] . "</option>";
            } endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_region') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="station_number">Номер участка</label>
        <input type="number" min="99999" class="form-control <?= ($validation->hasError('station_number')) ? 'is-invalid' : ''; ?>" name="station_number"
               value="<?= old('station_number'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('station_number') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="voter_number">Количество прикрепленных к участку избирателей</label>
        <input type="number" min="1" class="form-control <?= ($validation->hasError('voter_number')) ? 'is-invalid' : ''; ?>" name="voter_number"
               value="<?= old('voter_number'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('voter_number') ?>
        </div>

    </div>
      
    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>
  

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit" style="margin-left: auto; margin-right: auto;">Создать</button>
    </div>
    </form>


    </div>
<?= $this->endSection() ?>
