<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div  class="container main">


<h1>Все избирательные участки</h1>
    <br><h5> Добрый день, Администратор! На данной странице представлены избирательные участки в которых проводится голосование. </h5></br>
<?php if (!empty($all) && is_array($all)) : ?>
    
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group1','my_page') ?>
        <?= form_open('StationVoteController/viewAdmin', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>

        <?= form_open('StationVoteController/viewAdmin',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Регион" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>

    </div>

    <table class="table table-striped">
    <thead class="text-white bg-primary">
        <tr>
            <th scope="col"> Номер региона</th>
            <th scope="col"> Визитка</th>
            <th scope="col"> Наименование региона</th>

            <th scope="col"> Номер участка</th>
            <th scope="col"> Кол-во избирателей</th>
            <th scope="col"> Навигация</th>  
            <th scope="col"> Управление записями</th>            
           </tr>
    </thead>
    <tbody>
    <?php 
    foreach ($all as $item): ?>
        <tr>
            <th scope='row'><?php echo $item['id_region'] ?></th>
            <td>
                        <?php if (is_null($item['picture_url'])) : ?>
                            <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg" class="card-img">
                        <?php else:?>
                            <img height="50" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>                

            </td>
            <td><?php echo $item['name'] ?></td>

            <td><?php echo $item['station_number'] ?></td>
            <td><?php echo $item['voter_number'] ?></td>

            <td><a href="<?= base_url()?>/index.php/StationVoteController/view/<?= esc($item['id']); ?>" class="btn btn-outline-primary">  Просмотреть связанные  </a></td>
            <td><a class="btn btn-warning " href="<?= base_url()?>/index.php/StationVoteController/edit/<?php echo $item['id'] ?>">Редактировать</a>
                <a class="btn btn-danger " href="<?= base_url()?>/index.php/StationVoteController/delete/<?php echo $item['id'] ?>">Удалить </a>

            </td>
           
       </tr> 
    <?php endforeach; ?> 
    </tbody>
    </table>
    <?php else : ?>
        <p>Невозможно найти избирательные участки в которых проводится голосование.</p>
    <?php endif ?>
<br>
<br>
</div>

<?= $this->endSection() ?>
