<!DOCTYPE html>
<head>
    <title>Единый портал избирательных участков</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/yeti/bootstrap.min.css" integrity="sha384-mLBxp+1RMvmQmXOjBzRjqqr0dP9VHU2tb3FK6VB0fJN/AOu7/y+CAeYeWJZ4b3ii" crossorigin="anonymous">

    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        main{
            margin-top: 90px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <a class="navbar-brand" href="/index.php/pages/view/main">ВЫБОРЫ 2021</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Голосующие регионы
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="<?= base_url()?>/index.php/StationVoteController">Участки по регионам</a>
                    <?php if ($ionAuth->loggedIn()): ?>
                        <a class="dropdown-item" href="<?= base_url()?>/index.php/StationVoteController/create">Создание участка</a>
                    <?php endif ?>
                </div>
            </li> 
   
            <li class="nav-item dropdown">
                    <?php if ($ionAuth->loggedIn()): ?>
                        <a class="btn btn-primari btn-sm"  href="<?= base_url()?>/index.php/StationVoteController/viewAdmin"><font color="white">Все участки</font></a>
                    <?php endif ?>
            </li>
        
        </ul>
<div >
        <?php if (! $ionAuth->loggedIn()): ?>
            <a class="btn btn-outline-light" href="<?= base_url()?>/auth/login">Вход</a>
        <?php else: ?>
            <li class="nav-item dropdown" style="list-style-type:none">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:white"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;  <?php echo $ionAuth->user()->row()->email; ?></a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
                <a class="dropdown-item" href="<?= base_url()?>/auth/logout">Выход</a>
            </div>
            </li>
        <?php endif ?>
            </div>
    </div>
</nav>
<main role="main">


    <?php if (session()->getFlashdata('message')) :?>
        <div class="alert alert-info" role="alert" style="max-width: 540px; margin-left: auto; margin-right: auto;">
            <?= session()->getFlashdata('message') ?>
        </div>
    <?php endif ?>


<?= $this->renderSection('content') ?>
</main>
<br><br>
<footer class="text-center" >
    <p>© Lyantin Ilya 2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>/index.php/pages/view/agreement">Пользовательское соглашение</a></p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>

